// FileName: declarations.c V1.0
// Autor:    Carlos Cerda Jiménez
// Date:     
// Description: . 
/*======================================================================
						CODE DECLARATIONS
========================================================================*/

#include <float.h>
#include <stdio.h>
#include <math.h>
#include "library.h"
#include "library.c"

#define PII                       3.1415926535897932384626433832795
#define Ts                        ParamRealData(0,0)
#define n                         ParamRealData(0,1)
#define Vcell_grid                ParamRealData(0,2)
#define kp_average_balance        ParamRealData(0,3)
#define ki_average_balance        ParamRealData(0,4)
#define kp_VaS                    ParamRealData(0,5)
#define ki_VbS                    ParamRealData(0,6)
#define kp_V0D                    ParamRealData(0,7)
#define ki_V0D                    ParamRealData(0,8)
#define kp_VabD                   ParamRealData(0,9)
#define ki_VabD                   ParamRealData(0,10)
#define w_red                     ParamRealData(0,11)
#define kp_IabS                   ParamRealData(0,12)
#define ki_IabS                   ParamRealData(0,13)
#define kp_Iabc_dq                ParamRealData(0,14)
#define ki_Iabc_dq                ParamRealData(0,15)


//Vdc Reference
double Vdc_ref = 500;

//Reference for average_balance
double average_balance_ref;

//Theta park
double theta_Volt_entrada;

//variables for actuation for voltage control
double actuation_average_balance;
double actuation_control_VaS;
double actuation_control_VbS;
double actuation_control_VaD;
double actuation_control_VbD;
double actuation_control_V0D;

//variables for actuation for current control
double actuation_I_circulating_aS; //circulating current
double actuation_I_circulating_bS; //circulating current
double actuation_I_ACport_d; //AC port current
double actuation_I_ACport_q; //AC port current

//variables for low pass filters
double Low_pass_V_cap_clust_aS;
double Low_pass_V_cap_clust_bS;
double Low_pass_V_cap_clust_aD;
double Low_pass_V_cap_clust_bD;
double Low_pass_V_cap_clust_0D;

//Transform from abc to clarke
FORM_ABC Volt_trifasico_entrada;
FORM_CLARKE Volt_trifasico_entrada_Clarke;

//Transform for Iabc to clarke and park
 FORM_ABC Current_trifasico_entrada;
 FORM_CLARKE Current_trifasico_entrada_Clarke;
 FORM_PARK Current_trifasico_entrada_Park;

//Transform structure from abc to ab0SD
FORM_CLARKE_SIGMA_DELTA ab0SD_Cluster_current;
FORM_CLARKE_SIGMA_DELTA ab0SD_V_cap_cluster;

//Structures definition for measures
FORM_ABC_PN Cluster_current;//Structure definition for cluster currents
FORM_ABC_PN V_capacitor_cluster;//Structure definition for cluster voltage capacitor

//Structures for controllers
PI_form Control_Voltage_average_balance;//struct for PI used in average_balance (average cluster voltage)
PI_form Control_Voltage_alphaSigma;//struct for PI used in energy balance for voltage alphaSigma
PI_form Control_Voltage_betaSigma;//struct for PI used in energy balance for voltage betaSigma
PI_form Control_Voltage_alphaDelta;//struct for PI used in energy balance for voltage alphaSigma
PI_form Control_Voltage_betaDelta;//struct for PI used in energy balance for voltage betaSigma
PI_form Control_Voltage_ceroDelta;//struct for PI used in energy balance for voltage ceroDelta

//Structures for transformation after controllers
 //Transformation from dq to ab for Vc0D
FORM_PARK actuation_V0D_dq;
FORM_CLARKE actuation_V0D_ab;
FORM_PARK actuation_VabD_dq;
FORM_CLARKE actuation_VabD_ab;

//Structures for current controllers
PR_float_form Control_Current_IaS; //Circulating current
PR_float_form Control_Current_IbS; //circulating current
PI_form Control_Current_ACport_d; //AC port current
PI_form Control_Current_ACport_q; //AC port current

//Structures to transform from dq to Alpha Beta for AC port current
FORM_CLARKE Act_I_ACport_CLarke;
FORM_PARK Act_I_ACport_Park;

//Transformation from alph-beta to abcPN
FORM_CLARKE_SIGMA_DELTA Voltaje_ab0sD_Mod;
FORM_ABC_PN Voltaje_abcPN_Mod;

//Structures for filters
Filter V_capacitor_cluster_aS;//struct to low pass filter for VcCluster alphaSigma
Filter V_capacitor_cluster_bS;//struct to low pass filter for VcCluster betaSigma
Filter V_capacitor_cluster_aD;//struct to low pass filter for VcCluster alphaDelta
Filter V_capacitor_cluster_bD;//struct to low pass filter for VcCluster betaDelta
Filter V_capacitor_cluster_0D;//struct to low pass filter for VcCluster 0Delta

//Structure for balance of cells
Filter I_current_cluster_ap;//struct to low pass filter for Iap
Filter I_current_cluster_an;//struct to low pass filter for Ian
Filter I_current_cluster_bp;//struct to low pass filter for Ibp
Filter I_current_cluster_bn;//struct to low pass filter for Ibn
Filter I_current_cluster_cp;//struct to low pass filter for Icp
Filter I_current_cluster_cn;//struct to low pass filter for Icn

//Structures for measurements of voltages of capacitors in clusters
VC_CLUSTER_IND Voltaje_Capacitores_cluster_ap;
VC_CLUSTER_IND Voltaje_Capacitores_cluster_an;
VC_CLUSTER_IND Voltaje_Capacitores_cluster_bp;
VC_CLUSTER_IND Voltaje_Capacitores_cluster_bn;
VC_CLUSTER_IND Voltaje_Capacitores_cluster_cp;
VC_CLUSTER_IND Voltaje_Capacitores_cluster_cn;

//variables for balance of cells
double k_cell = 0.1; //Proportional gain for balance of cells

double I_current_cluster_ap_atan; //This is to get the sign of the cluster current filtered smoothed
double I_current_cluster_an_atan; //This is to get the sign of the cluster current filtered smoothed
double I_current_cluster_bp_atan; //This is to get the sign of the cluster current filtered smoothed
double I_current_cluster_bn_atan; //This is to get the sign of the cluster current filtered smoothed
double I_current_cluster_cp_atan; //This is to get the sign of the cluster current filtered smoothed
double I_current_cluster_cn_atan; //This is to get the sign of the cluster current filtered smoothed


//Structures for controlling voltage capacitors in balance of cells
P_CONTROL_VOLTAGE_CAPACITORS Actuation_Voltage_ap;
P_CONTROL_VOLTAGE_CAPACITORS Actuation_Voltage_an;
P_CONTROL_VOLTAGE_CAPACITORS Actuation_Voltage_bp;
P_CONTROL_VOLTAGE_CAPACITORS Actuation_Voltage_bn;
P_CONTROL_VOLTAGE_CAPACITORS Actuation_Voltage_cp;
P_CONTROL_VOLTAGE_CAPACITORS Actuation_Voltage_cn;