// FileName: Output.c V1.0
// Autor:    Carlos Cerda Jiménez
// Date:     
// Description: . 
/*======================================================================
						OUTPUT FUNCTION CODE
========================================================================*/
//Assignation of measures to the structures for cluster currents
Cluster_current.ap = InputSignal(0,0);
Cluster_current.bp = InputSignal(0,1);
Cluster_current.cp = InputSignal(0,2);
Cluster_current.an = InputSignal(0,3);
Cluster_current.bn = InputSignal(0,4);
Cluster_current.cn = InputSignal(0,5);
//Assignation of measures to the structures for cluster voltages
V_capacitor_cluster.ap = InputSignal(1,0);
V_capacitor_cluster.bp = InputSignal(1,1);
V_capacitor_cluster.cp = InputSignal(1,2);
V_capacitor_cluster.an = InputSignal(1,3);
V_capacitor_cluster.bn = InputSignal(1,4);
V_capacitor_cluster.cn = InputSignal(1,5);

//AC port inputs
Volt_trifasico_entrada.a = InputSignal(2,0);
Volt_trifasico_entrada.b = InputSignal(2,1);
Volt_trifasico_entrada.c = InputSignal(2,2);

Current_trifasico_entrada.a = InputSignal(3,0);
Current_trifasico_entrada.b = InputSignal(3,1);
Current_trifasico_entrada.c = InputSignal(3,2);

//Measures of voltages of capacitors in clusters
Voltaje_Capacitores_cluster_ap.vc1 = InputSignal(4,0);
Voltaje_Capacitores_cluster_ap.vc2 = InputSignal(4,1);
Voltaje_Capacitores_cluster_ap.vc3 = InputSignal(4,2);
// Voltaje_Capacitores_cluster_ap.vc4 = InputSignal(4,3);
// Voltaje_Capacitores_cluster_ap.vc5 = InputSignal(4,4);
// Voltaje_Capacitores_cluster_ap.vc6 = InputSignal(4,5);

Voltaje_Capacitores_cluster_an.vc1 = InputSignal(5,0);
Voltaje_Capacitores_cluster_an.vc2 = InputSignal(5,1);
Voltaje_Capacitores_cluster_an.vc3 = InputSignal(5,2);
// Voltaje_Capacitores_cluster_an.vc4 = InputSignal(5,3);
// Voltaje_Capacitores_cluster_an.vc5 = InputSignal(5,4);
// Voltaje_Capacitores_cluster_an.vc6 = InputSignal(5,5);

Voltaje_Capacitores_cluster_bp.vc1 = InputSignal(6,0);
Voltaje_Capacitores_cluster_bp.vc2 = InputSignal(6,1);
Voltaje_Capacitores_cluster_bp.vc3 = InputSignal(6,2);
// Voltaje_Capacitores_cluster_bp.vc4 = InputSignal(6,3);
// Voltaje_Capacitores_cluster_bp.vc5 = InputSignal(6,4);
// Voltaje_Capacitores_cluster_bp.vc6 = InputSignal(6,5);

Voltaje_Capacitores_cluster_bn.vc1 = InputSignal(7,0);
Voltaje_Capacitores_cluster_bn.vc2 = InputSignal(7,1);
Voltaje_Capacitores_cluster_bn.vc3 = InputSignal(7,2);
// Voltaje_Capacitores_cluster_bn.vc4 = InputSignal(7,3);
// Voltaje_Capacitores_cluster_bn.vc5 = InputSignal(7,4);
// Voltaje_Capacitores_cluster_bn.vc6 = InputSignal(7,5);

Voltaje_Capacitores_cluster_cp.vc1 = InputSignal(8,0);
Voltaje_Capacitores_cluster_cp.vc2 = InputSignal(8,1);
Voltaje_Capacitores_cluster_cp.vc3 = InputSignal(8,2);
// Voltaje_Capacitores_cluster_cp.vc4 = InputSignal(8,3);
// Voltaje_Capacitores_cluster_cp.vc5 = InputSignal(8,4);
// Voltaje_Capacitores_cluster_cp.vc6 = InputSignal(8,5);

Voltaje_Capacitores_cluster_cn.vc1 = InputSignal(9,0);
Voltaje_Capacitores_cluster_cn.vc2 = InputSignal(9,1);
Voltaje_Capacitores_cluster_cn.vc3 = InputSignal(9,2);
// Voltaje_Capacitores_cluster_cn.vc4 = InputSignal(9,3);
// Voltaje_Capacitores_cluster_cn.vc5 = InputSignal(9,4);
// Voltaje_Capacitores_cluster_cn.vc6 = InputSignal(9,5);


//Reference for average_balance
average_balance_ref = n * Vcell_grid;

//Obtaining theta
Transform_ABC2Clarke(Volt_trifasico_entrada, &Volt_trifasico_entrada_Clarke);
theta_Volt_entrada = atan2(Volt_trifasico_entrada_Clarke.beta,Volt_trifasico_entrada_Clarke.alpha);
//Clarke and Park for AC port current
Transform_ABC2Clarke(Current_trifasico_entrada, &Current_trifasico_entrada_Clarke);
Transform_Clarke2Park(Current_trifasico_entrada_Clarke, &Current_trifasico_entrada_Park, theta_Volt_entrada);

//calling for transform abc to abd0SD
Transform_ABCPN2AB0SD(Cluster_current, &ab0SD_Cluster_current);
Transform_ABCPN2AB0SD(V_capacitor_cluster, &ab0SD_V_cap_cluster);

//Actuation for control average_balance
actuation_average_balance = CalculatePI_tustin(&Control_Voltage_average_balance,average_balance_ref,ab0SD_V_cap_cluster.ceroSigma);

//Low pass filter
gen_filter(&V_capacitor_cluster_aS,ab0SD_V_cap_cluster.alphaSigma,&V_capacitor_cluster_aS.out);
gen_filter(&V_capacitor_cluster_bS,ab0SD_V_cap_cluster.betaSigma,&V_capacitor_cluster_bS.out);
gen_filter(&V_capacitor_cluster_aD,ab0SD_V_cap_cluster.alphaDelta,&V_capacitor_cluster_aD.out);
gen_filter(&V_capacitor_cluster_bD,ab0SD_V_cap_cluster.betaDelta,&V_capacitor_cluster_bD.out);
gen_filter(&V_capacitor_cluster_0D,ab0SD_V_cap_cluster.ceroDelta,&V_capacitor_cluster_0D.out);

Low_pass_V_cap_clust_aS = V_capacitor_cluster_aS.out;
Low_pass_V_cap_clust_bS = V_capacitor_cluster_bS.out;
Low_pass_V_cap_clust_aD = V_capacitor_cluster_aD.out;
Low_pass_V_cap_clust_bD = V_capacitor_cluster_bD.out;
Low_pass_V_cap_clust_0D = V_capacitor_cluster_0D.out;

//Actuation for Energy balance
actuation_control_VaS = CalculatePI_tustin(&Control_Voltage_alphaSigma,0,Low_pass_V_cap_clust_aS);
actuation_control_VbS = CalculatePI_tustin(&Control_Voltage_betaSigma,0,Low_pass_V_cap_clust_bS);
actuation_control_VaD = CalculatePI_tustin(&Control_Voltage_alphaDelta,0,Low_pass_V_cap_clust_aD);
actuation_control_VbD = CalculatePI_tustin(&Control_Voltage_betaDelta,0,Low_pass_V_cap_clust_bD);
actuation_control_V0D = CalculatePI_tustin(&Control_Voltage_ceroDelta,0,Low_pass_V_cap_clust_0D);

//Transformation from dq to alpha beta for Vc0D and VcabD
actuation_V0D_dq.d = -actuation_control_V0D;
actuation_V0D_dq.q = 0;
Transform_Park2Clarke(actuation_V0D_dq ,&actuation_V0D_ab , theta_Volt_entrada);// Transformation from dq to ab for Vc0D

actuation_VabD_dq.d = -actuation_control_VaD;
actuation_VabD_dq.q = actuation_control_VbD;
Transform_Park2Clarke(actuation_VabD_dq ,&actuation_VabD_ab , -theta_Volt_entrada);// Transformation from dq to ab for VcabD

//Current Control
actuation_I_circulating_aS = CalculatePR_tpw(&Control_Current_IaS, actuation_control_VaS+actuation_V0D_ab.alpha+actuation_VabD_ab.alpha, ab0SD_Cluster_current.alphaSigma); //Circulating current
actuation_I_circulating_bS = CalculatePR_tpw(&Control_Current_IbS, actuation_control_VbS+actuation_V0D_ab.beta+actuation_VabD_ab.beta, ab0SD_Cluster_current.betaSigma); //Circulating current

actuation_I_ACport_d = CalculatePI_tustin(&Control_Current_ACport_d, -actuation_average_balance, Current_trifasico_entrada_Park.d); //AC port current d component
actuation_I_ACport_q = CalculatePI_tustin(&Control_Current_ACport_q, 0, Current_trifasico_entrada_Park.q); //AC port current q component

Act_I_ACport_Park.d = -actuation_I_ACport_d;
Act_I_ACport_Park.q = -actuation_I_ACport_q;


Transform_Park2Clarke(Act_I_ACport_Park ,&Act_I_ACport_CLarke , theta_Volt_entrada);
Voltaje_ab0sD_Mod.alphaSigma = -actuation_I_circulating_aS;
Voltaje_ab0sD_Mod.betaSigma = -actuation_I_circulating_bS;
Voltaje_ab0sD_Mod.ceroSigma = Vdc_ref * 0.5;//E_ref/2
Voltaje_ab0sD_Mod.alphaDelta = Act_I_ACport_CLarke.alpha;
Voltaje_ab0sD_Mod.betaDelta = Act_I_ACport_CLarke.beta;
Voltaje_ab0sD_Mod.ceroDelta = 0;//Vcm

Transform_AB0SD2ABCPN(Voltaje_ab0sD_Mod, &Voltaje_abcPN_Mod);


//Balance of cells
//Low pass filter for cluster currents
gen_filter(&I_current_cluster_ap,Cluster_current.ap,&I_current_cluster_ap.out);
gen_filter(&I_current_cluster_an,Cluster_current.an,&I_current_cluster_an.out);
gen_filter(&I_current_cluster_bp,Cluster_current.bp,&I_current_cluster_bp.out);
gen_filter(&I_current_cluster_bn,Cluster_current.bn,&I_current_cluster_bn.out);
gen_filter(&I_current_cluster_cp,Cluster_current.cp,&I_current_cluster_cp.out);
gen_filter(&I_current_cluster_cn,Cluster_current.cn,&I_current_cluster_cn.out);

I_current_cluster_ap_atan = atan(10 * I_current_cluster_ap.out)*2/PII; //This is to get the sign of the cluster current filtered smoothed
I_current_cluster_an_atan = atan(10 * I_current_cluster_an.out)*2/PII; //This is to get the sign of the cluster current filtered smoothed
I_current_cluster_bp_atan = atan(10 * I_current_cluster_bp.out)*2/PII; //This is to get the sign of the cluster current filtered smoothed
I_current_cluster_bn_atan = atan(10 * I_current_cluster_bn.out)*2/PII; //This is to get the sign of the cluster current filtered smoothed
I_current_cluster_cp_atan = atan(10 * I_current_cluster_cp.out)*2/PII; //This is to get the sign of the cluster current filtered smoothed
I_current_cluster_cn_atan = atan(10 * I_current_cluster_cn.out)*2/PII; //This is to get the sign of the cluster current filtered smoothed


Actuation_Voltage_ap.vc1 = (Vcell_grid - Voltaje_Capacitores_cluster_ap.vc1) * k_cell; //Error of cell times k_cell (proportional control)
Actuation_Voltage_ap.vc2 = (Vcell_grid - Voltaje_Capacitores_cluster_ap.vc2) * k_cell; //Error of cell times k_cell (proportional control)
Actuation_Voltage_ap.vc3 = (Vcell_grid - Voltaje_Capacitores_cluster_ap.vc3) * k_cell; //Error of cell times k_cell (proportional control)
// Actuation_Voltage_ap.vc4 = (Vcell_grid - Voltaje_Capacitores_cluster_ap.vc4) * k_cell; //Error of cell times k_cell (proportional control)
// Actuation_Voltage_ap.vc5 = (Vcell_grid - Voltaje_Capacitores_cluster_ap.vc5) * k_cell; //Error of cell times k_cell (proportional control)
// Actuation_Voltage_ap.vc6 = (Vcell_grid - Voltaje_Capacitores_cluster_ap.vc6) * k_cell; //Error of cell times k_cell (proportional control)

Actuation_Voltage_an.vc1 = (Vcell_grid - Voltaje_Capacitores_cluster_an.vc1) * k_cell; //Error of cell times k_cell (proportional control)
Actuation_Voltage_an.vc2 = (Vcell_grid - Voltaje_Capacitores_cluster_an.vc2) * k_cell; //Error of cell times k_cell (proportional control)
Actuation_Voltage_an.vc3 = (Vcell_grid - Voltaje_Capacitores_cluster_an.vc3) * k_cell; //Error of cell times k_cell (proportional control)
// Actuation_Voltage_an.vc4 = (Vcell_grid - Voltaje_Capacitores_cluster_an.vc4) * k_cell; //Error of cell times k_cell (proportional control)
// Actuation_Voltage_an.vc5 = (Vcell_grid - Voltaje_Capacitores_cluster_an.vc5) * k_cell; //Error of cell times k_cell (proportional control)
// Actuation_Voltage_an.vc6 = (Vcell_grid - Voltaje_Capacitores_cluster_an.vc6) * k_cell; //Error of cell times k_cell (proportional control)

Actuation_Voltage_bp.vc1 = (Vcell_grid - Voltaje_Capacitores_cluster_bp.vc1) * k_cell; //Error of cell times k_cell (proportional control)
Actuation_Voltage_bp.vc2 = (Vcell_grid - Voltaje_Capacitores_cluster_bp.vc2) * k_cell; //Error of cell times k_cell (proportional control)
Actuation_Voltage_bp.vc3 = (Vcell_grid - Voltaje_Capacitores_cluster_bp.vc3) * k_cell; //Error of cell times k_cell (proportional control)
// Actuation_Voltage_bp.vc4 = (Vcell_grid - Voltaje_Capacitores_cluster_bp.vc4) * k_cell; //Error of cell times k_cell (proportional control)
// Actuation_Voltage_bp.vc5 = (Vcell_grid - Voltaje_Capacitores_cluster_bp.vc5) * k_cell; //Error of cell times k_cell (proportional control)
// Actuation_Voltage_bp.vc6 = (Vcell_grid - Voltaje_Capacitores_cluster_bp.vc6) * k_cell; //Error of cell times k_cell (proportional control)

Actuation_Voltage_bn.vc1 = (Vcell_grid - Voltaje_Capacitores_cluster_bn.vc1) * k_cell; //Error of cell times k_cell (proportional control)
Actuation_Voltage_bn.vc2 = (Vcell_grid - Voltaje_Capacitores_cluster_bn.vc2) * k_cell; //Error of cell times k_cell (proportional control)
Actuation_Voltage_bn.vc3 = (Vcell_grid - Voltaje_Capacitores_cluster_bn.vc3) * k_cell; //Error of cell times k_cell (proportional control)
// Actuation_Voltage_bn.vc4 = (Vcell_grid - Voltaje_Capacitores_cluster_bn.vc4) * k_cell; //Error of cell times k_cell (proportional control)
// Actuation_Voltage_bn.vc5 = (Vcell_grid - Voltaje_Capacitores_cluster_bn.vc5) * k_cell; //Error of cell times k_cell (proportional control)
// Actuation_Voltage_bn.vc6 = (Vcell_grid - Voltaje_Capacitores_cluster_bn.vc6) * k_cell; //Error of cell times k_cell (proportional control)

Actuation_Voltage_cp.vc1 = (Vcell_grid - Voltaje_Capacitores_cluster_cp.vc1) * k_cell; //Error of cell times k_cell (proportional control)
Actuation_Voltage_cp.vc2 = (Vcell_grid - Voltaje_Capacitores_cluster_cp.vc2) * k_cell; //Error of cell times k_cell (proportional control)
Actuation_Voltage_cp.vc3 = (Vcell_grid - Voltaje_Capacitores_cluster_cp.vc3) * k_cell; //Error of cell times k_cell (proportional control)
// Actuation_Voltage_cp.vc4 = (Vcell_grid - Voltaje_Capacitores_cluster_cp.vc4) * k_cell; //Error of cell times k_cell (proportional control)
// Actuation_Voltage_cp.vc5 = (Vcell_grid - Voltaje_Capacitores_cluster_cp.vc5) * k_cell; //Error of cell times k_cell (proportional control)
// Actuation_Voltage_cp.vc6 = (Vcell_grid - Voltaje_Capacitores_cluster_cp.vc6) * k_cell; //Error of cell times k_cell (proportional control)

Actuation_Voltage_cn.vc1 = (Vcell_grid - Voltaje_Capacitores_cluster_cn.vc1) * k_cell; //Error of cell times k_cell (proportional control)
Actuation_Voltage_cn.vc2 = (Vcell_grid - Voltaje_Capacitores_cluster_cn.vc2) * k_cell; //Error of cell times k_cell (proportional control)
Actuation_Voltage_cn.vc3 = (Vcell_grid - Voltaje_Capacitores_cluster_cn.vc3) * k_cell; //Error of cell times k_cell (proportional control)
// Actuation_Voltage_cn.vc4 = (Vcell_grid - Voltaje_Capacitores_cluster_cn.vc4) * k_cell; //Error of cell times k_cell (proportional control)
// Actuation_Voltage_cn.vc5 = (Vcell_grid - Voltaje_Capacitores_cluster_cn.vc5) * k_cell; //Error of cell times k_cell (proportional control)
// Actuation_Voltage_cn.vc6 = (Vcell_grid - Voltaje_Capacitores_cluster_cn.vc6) * k_cell; //Error of cell times k_cell (proportional control)



//Ouputs signals

OutputSignal(0,0) = ((Actuation_Voltage_ap.vc1 * I_current_cluster_ap_atan) + Voltaje_abcPN_Mod.ap/n) / Voltaje_Capacitores_cluster_ap.vc1;
OutputSignal(0,1) = ((Actuation_Voltage_ap.vc2 * I_current_cluster_ap_atan) + Voltaje_abcPN_Mod.ap/n) / Voltaje_Capacitores_cluster_ap.vc2;
OutputSignal(0,2) = ((Actuation_Voltage_ap.vc3 * I_current_cluster_ap_atan) + Voltaje_abcPN_Mod.ap/n) / Voltaje_Capacitores_cluster_ap.vc3;
// OutputSignal(0,3) = ((Actuation_Voltage_ap.vc4 * I_current_cluster_ap_atan) + Voltaje_abcPN_Mod.ap/n) / Voltaje_Capacitores_cluster_ap.vc4;
// OutputSignal(0,4) = ((Actuation_Voltage_ap.vc5 * I_current_cluster_ap_atan) + Voltaje_abcPN_Mod.ap/n) / Voltaje_Capacitores_cluster_ap.vc5;
// OutputSignal(0,5) = ((Actuation_Voltage_ap.vc6 * I_current_cluster_ap_atan) + Voltaje_abcPN_Mod.ap/n) / Voltaje_Capacitores_cluster_ap.vc6;

OutputSignal(1,0) = ((Actuation_Voltage_an.vc1 * I_current_cluster_an_atan) + Voltaje_abcPN_Mod.an/n) / Voltaje_Capacitores_cluster_an.vc1;
OutputSignal(1,1) = ((Actuation_Voltage_an.vc2 * I_current_cluster_an_atan) + Voltaje_abcPN_Mod.an/n) / Voltaje_Capacitores_cluster_an.vc2;
OutputSignal(1,2) = ((Actuation_Voltage_an.vc3 * I_current_cluster_an_atan) + Voltaje_abcPN_Mod.an/n) / Voltaje_Capacitores_cluster_an.vc3;
// OutputSignal(1,3) = ((Actuation_Voltage_an.vc4 * I_current_cluster_an_atan) + Voltaje_abcPN_Mod.an/n) / Voltaje_Capacitores_cluster_an.vc4;
// OutputSignal(1,4) = ((Actuation_Voltage_an.vc5 * I_current_cluster_an_atan) + Voltaje_abcPN_Mod.an/n) / Voltaje_Capacitores_cluster_an.vc5;
// OutputSignal(1,5) = ((Actuation_Voltage_an.vc6 * I_current_cluster_an_atan) + Voltaje_abcPN_Mod.an/n) / Voltaje_Capacitores_cluster_an.vc6;

OutputSignal(2,0) = ((Actuation_Voltage_bp.vc1 * I_current_cluster_bp_atan) + Voltaje_abcPN_Mod.bp/n) / Voltaje_Capacitores_cluster_bp.vc1;
OutputSignal(2,1) = ((Actuation_Voltage_bp.vc2 * I_current_cluster_bp_atan) + Voltaje_abcPN_Mod.bp/n) / Voltaje_Capacitores_cluster_bp.vc2;
OutputSignal(2,2) = ((Actuation_Voltage_bp.vc3 * I_current_cluster_bp_atan) + Voltaje_abcPN_Mod.bp/n) / Voltaje_Capacitores_cluster_bp.vc3;
// OutputSignal(2,3) = ((Actuation_Voltage_bp.vc4 * I_current_cluster_bp_atan) + Voltaje_abcPN_Mod.bp/n) / Voltaje_Capacitores_cluster_bp.vc4;
// OutputSignal(2,4) = ((Actuation_Voltage_bp.vc5 * I_current_cluster_bp_atan) + Voltaje_abcPN_Mod.bp/n) / Voltaje_Capacitores_cluster_bp.vc5;
// OutputSignal(2,5) = ((Actuation_Voltage_bp.vc6 * I_current_cluster_bp_atan) + Voltaje_abcPN_Mod.bp/n) / Voltaje_Capacitores_cluster_bp.vc6;

OutputSignal(3,0) = ((Actuation_Voltage_bn.vc1 * I_current_cluster_bn_atan) + Voltaje_abcPN_Mod.bn/n) / Voltaje_Capacitores_cluster_bn.vc1;
OutputSignal(3,1) = ((Actuation_Voltage_bn.vc2 * I_current_cluster_bn_atan) + Voltaje_abcPN_Mod.bn/n) / Voltaje_Capacitores_cluster_bn.vc2;
OutputSignal(3,2) = ((Actuation_Voltage_bn.vc3 * I_current_cluster_bn_atan) + Voltaje_abcPN_Mod.bn/n) / Voltaje_Capacitores_cluster_bn.vc3;
// OutputSignal(3,3) = ((Actuation_Voltage_bn.vc4 * I_current_cluster_bn_atan) + Voltaje_abcPN_Mod.bn/n) / Voltaje_Capacitores_cluster_bn.vc4;
// OutputSignal(3,4) = ((Actuation_Voltage_bn.vc5 * I_current_cluster_bn_atan) + Voltaje_abcPN_Mod.bn/n) / Voltaje_Capacitores_cluster_bn.vc5;
// OutputSignal(3,5) = ((Actuation_Voltage_bn.vc6 * I_current_cluster_bn_atan) + Voltaje_abcPN_Mod.bn/n) / Voltaje_Capacitores_cluster_bn.vc6;

OutputSignal(4,0) = ((Actuation_Voltage_cp.vc1 * I_current_cluster_cp_atan) + Voltaje_abcPN_Mod.cp/n) / Voltaje_Capacitores_cluster_cp.vc1;
OutputSignal(4,1) = ((Actuation_Voltage_cp.vc2 * I_current_cluster_cp_atan) + Voltaje_abcPN_Mod.cp/n) / Voltaje_Capacitores_cluster_cp.vc2;
OutputSignal(4,2) = ((Actuation_Voltage_cp.vc3 * I_current_cluster_cp_atan) + Voltaje_abcPN_Mod.cp/n) / Voltaje_Capacitores_cluster_cp.vc3;
// OutputSignal(4,3) = ((Actuation_Voltage_cp.vc4 * I_current_cluster_cp_atan) + Voltaje_abcPN_Mod.cp/n) / Voltaje_Capacitores_cluster_cp.vc4;
// OutputSignal(4,4) = ((Actuation_Voltage_cp.vc5 * I_current_cluster_cp_atan) + Voltaje_abcPN_Mod.cp/n) / Voltaje_Capacitores_cluster_cp.vc5;
// OutputSignal(4,5) = ((Actuation_Voltage_cp.vc6 * I_current_cluster_cp_atan) + Voltaje_abcPN_Mod.cp/n) / Voltaje_Capacitores_cluster_cp.vc6;

OutputSignal(5,0) = ((Actuation_Voltage_cn.vc1 * I_current_cluster_cn_atan) + Voltaje_abcPN_Mod.cn/n) / Voltaje_Capacitores_cluster_cn.vc1;
OutputSignal(5,1) = ((Actuation_Voltage_cn.vc2 * I_current_cluster_cn_atan) + Voltaje_abcPN_Mod.cn/n) / Voltaje_Capacitores_cluster_cn.vc2;
OutputSignal(5,2) = ((Actuation_Voltage_cn.vc3 * I_current_cluster_cn_atan) + Voltaje_abcPN_Mod.cn/n) / Voltaje_Capacitores_cluster_cn.vc3;
// OutputSignal(5,3) = ((Actuation_Voltage_cn.vc4 * I_current_cluster_cn_atan) + Voltaje_abcPN_Mod.cn/n) / Voltaje_Capacitores_cluster_cn.vc4;
// OutputSignal(5,4) = ((Actuation_Voltage_cn.vc5 * I_current_cluster_cn_atan) + Voltaje_abcPN_Mod.cn/n) / Voltaje_Capacitores_cluster_cn.vc5;
// OutputSignal(5,5) = ((Actuation_Voltage_cn.vc6 * I_current_cluster_cn_atan) + Voltaje_abcPN_Mod.cn/n) / Voltaje_Capacitores_cluster_cn.vc6;