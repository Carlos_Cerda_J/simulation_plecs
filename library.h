// FileName: library.c V1.0
// Autor:    Carlos Cerda Jiménez
// Date:     
// Description: . 
/*======================================================================
						library declarations
========================================================================*/

//Structures declarations

typedef struct FORM_CLARKE_SIGMA_DELTA
{
	double alphaSigma;
	double betaSigma;
	double ceroSigma;
	double alphaDelta;
	double betaDelta;
	double ceroDelta;

} FORM_CLARKE_SIGMA_DELTA;

typedef struct FORM_ABC_PN
{
	double ap;
	double bp;
	double cp;
	double an;
	double bn;
	double cn;

} FORM_ABC_PN;

typedef struct PI_form{
	float out_act;
	float error_act;
	float out_1;
	float error_1;
	double kpz;
	double az;
	float umax;
}PI_form;

typedef struct PR_float_form{

	float error_act;
	float out_act;
	float error_1;
	float error_2;
	float out_1;
	float out_2;
	float a0;
	float a1;
	float a2;
	float b1;
	float b2;

}PR_float_form;


typedef struct VC_CLUSTER_IND{
	//Structure for measured voltage of every capacitor in the cell
	float vc1;
	float vc2;
	float vc3;
	float vc4;
	float vc5;
	float vc6;
}VC_CLUSTER_IND;

typedef struct P_CONTROL_VOLTAGE_CAPACITORS{
	//Structure created to save the actuation of proportional controller for
	// balance of cells in the M2C
	float vc1;
	float vc2;
	float vc3;
	float vc4;
	float vc5;
	float vc6;
}P_CONTROL_VOLTAGE_CAPACITORS;

typedef struct PR_float_AnC{

	float error_act;
	float out_act;
	float error_1;
	float error_2;
	float out_1;
	float out_2;
	float a0;
	float a1;
	float a2;
	float b1;
	float b2;

}PR_float_AnC;

//structure for filter low pass butterworth
typedef struct Filter{
	float a0;
	float a1;
	float a2;
	float b0;
	float b1;
	float b2;
	float in;
	float out;
	float in_1;
	float in_2;
	float out_1;
	float out_2;
	float h;
	float fc;
}Filter;

//Structure for Clarke transformation

typedef struct FORM_ABC
{
	double a;
	double b;
	double c;
} FORM_ABC;

typedef struct FORM_CLARKE
{
	double alpha;
	double beta;
	double cero;
} FORM_CLARKE;

//Structure for Park transformation
typedef struct FORM_PARK
{
	double d;
	double q;
	double cero;
} FORM_PARK;


//Functions declarations
//Transformations
void Transform_ABCPN2AB0SD(FORM_ABC_PN A, FORM_CLARKE_SIGMA_DELTA *B);
void Transform_AB0SD2ABCPN(FORM_CLARKE_SIGMA_DELTA A, FORM_ABC_PN *B);
void Transform_ABC2Clarke(FORM_ABC A, FORM_CLARKE *B);
void Transform_Clarke2ABC(FORM_CLARKE A, FORM_ABC *B);
void Transform_Clarke2Park(FORM_CLARKE A, FORM_PARK *B, double theta);
void Transform_Park2Clarke(FORM_PARK A, FORM_CLARKE *B, double theta);

//Controllers
void InitializePI_tustin(PI_form *PI, float kp, float ki, float ts, float lim);
double CalculatePI_tustin(PI_form *PI,float ref, float meas);

void InitializePR_tpw(PR_float_form *PR, float kp, float ki, float w, float ts);
double CalculatePR_tpw(PR_float_form *PR,float ref, float meas);

void InitializePR_tpw_AnC(PR_float_AnC *PRAnC, float kp, float ki, float w, float ts, float Dn);
double CalculatePR_tpw_AnC(PR_float_AnC *PRAnC, float ref, float meas);

//Filters
void init_butt_filter(Filter *F, float fc,float h); //Low pass filter
void init_zero_butt_filter(Filter *F, float fc,float h); //Low pass filter
void gen_filter(Filter *F,float in, float *out); //Low pass filter