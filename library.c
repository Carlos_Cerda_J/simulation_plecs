// FileName: library.h V1.0
// Autor:    Carlos Cerda Jiménez
// Date:     
// Description: . 
/*======================================================================
						library definitions
========================================================================*/


//Constant values used for transformations
const double THREE_INV		= 0.33333333333333333333333333333333;	// 1/3
const double SQRT_THREE	= 1.7320508075688772935274463415059;		// 1/sqrt(3)
const double TWO_SQRT_THREE = 3.4641016151377545870548926830117;		// 2*sqrt(3)
const double SQRT_THREE_INV	= 0.57735026918962576450914878050196;	// 1/sqrt(3)
const double PI_NUMBER	= 3.1415926535897932384626433832795;


void Transform_ABCPN2AB0SD(FORM_ABC_PN A,FORM_CLARKE_SIGMA_DELTA *B)
{
	//Transformation from 3 phase variables to alpha beta sigma delta
	B->alphaSigma = 0.5 * THREE_INV * (2.0 * A.ap + 2.0 * A.an - A.bp - A.bn - A.cp - A.cn);
	B->betaSigma  = 0.5 * SQRT_THREE_INV * (A.bp + A.bn - A.cp - A.cn);//Negative sequence 0.288675 * (-X_bP - X_bN + X_cP + X_cN);
	B->ceroSigma  = 0.5 * THREE_INV * (A.ap + A.an + A.bp + A.bn + A.cp + A.cn);
	B->alphaDelta = THREE_INV * (-2.0 * A.an + A.bn + A.cn + 2.0 * A.ap - A.bp - A.cp);
	B->betaDelta  = SQRT_THREE_INV * (A.bp - A.bn - A.cp + A.cn);//Negative sequence 0.577350 * (-X_bP + X_bN + X_cP - X_cN);
	B->ceroDelta  = THREE_INV * (A.ap - A.an + A.bp - A.bn + A.cp - A.cn);
}

void Transform_AB0SD2ABCPN(FORM_CLARKE_SIGMA_DELTA A, FORM_ABC_PN *B)
{
	B->ap = (A.alphaSigma + 0.5 * A.alphaDelta + A.ceroSigma + 0.5 * A.ceroDelta);
	B->bp = 0.25 * (-2.0 * A.alphaSigma - A.alphaDelta + TWO_SQRT_THREE * A.betaSigma + SQRT_THREE * A.betaDelta + 4.0 * A.ceroSigma + 2.0 * A.ceroDelta);
	B->cp = 0.25 * (-2.0 * A.alphaSigma - A.alphaDelta - TWO_SQRT_THREE * A.betaSigma - SQRT_THREE * A.betaDelta + 4.0 * A.ceroSigma + 2.0 * A.ceroDelta);
	B->an = (A.alphaSigma - 0.5 * A.alphaDelta + A.ceroSigma - 0.5 * A.ceroDelta);
	B->bn = 0.25 * (- 2.0 * A.alphaSigma + A.alphaDelta + TWO_SQRT_THREE * A.betaSigma - SQRT_THREE * A.betaDelta + 4.0 * A.ceroSigma - 2.0 * A.ceroDelta);
	B->cn = 0.25 * (- 2.0 * A.alphaSigma + A.alphaDelta - TWO_SQRT_THREE * A.betaSigma + SQRT_THREE * A.betaDelta + 4.0 * A.ceroSigma - 2.0 * A.ceroDelta);
}


//Controllers Initialization
void InitializePI_tustin(PI_form *PI, float kp, float ki, float ts, float lim)
{
	float a, kpz_aux, az_aux;
	a = ki/kp;
	kpz_aux = (2.0+a*ts)*kp/2.0;
	az_aux  = (2.0-a*ts)/(2.0+a*ts);

	PI->out_act = 0;
	PI->error_act = 0;
	PI->out_1 = 0;
	PI->error_1 = 0;
	PI->kpz = kpz_aux;
	PI->az = az_aux;
	PI->umax = lim;
}

void InitializePR_tpw(PR_float_form *PR, float kp, float ki, float w, float ts)
{

	float b1_aux;
	float b2_aux;
	float a0_aux;
	float a1_aux;
	float a2_aux;

	b1_aux = -2.0 * cos(ts*w);
	b2_aux = 1.0;
	a0_aux = kp + (ki * sin(ts*w) / (2.0*w));
	a1_aux = -2.0 * kp * cos(ts*w);
	a2_aux = kp - (ki * sin(ts*w) / (2.0*w));

	PR->out_act = 0;
	PR->error_act = 0;
	PR->out_1 = 0;
	PR->out_2 = 0;
	PR->error_1 = 0;
	PR->error_2 = 0;
	PR->a0 = a0_aux;
	PR->a1 = a1_aux;
	PR->a2 = a2_aux;
	PR->b1 = b1_aux;
	PR->b2 = b2_aux;

}

void InitializePR_tpw_AnC(PR_float_AnC *PRAnC, float kp, float ki, float w, float ts, float Dn)
{//Initialization for PR prewarping with angle compensation

	float b1_aux;
	float b2_aux;
	float a0_aux;
	float a1_aux;
	float a2_aux;

	b1_aux = -2.0 * cos(ts*w);
	b2_aux = 1.0;
	a0_aux = kp + ((ki / (2 * w)) * cos(ts*w*Dn) * sin(ts*w)) - ((ki / w) * sin(ts*w*Dn) * sin(ts*w*0.5) * sin(ts*w*0.5));
	a1_aux = -2.0 * kp * cos(ts*w) - (2.0 * (ki / w) * sin(ts*w*Dn) * sin(ts*w*0.5) * sin(ts*w*0.5));
	a2_aux = kp - ((ki / (2 * w)) * cos(ts*w*Dn) * sin (ts*w)) - ((ki / w) * sin(ts*w*Dn) * sin(ts*w*0.5) * sin(ts*w*0.5));

	PRAnC->out_act = 0;
	PRAnC->error_act = 0;
	PRAnC->out_1 = 0;
	PRAnC->out_2 = 0;
	PRAnC->error_1 = 0;
	PRAnC->error_2 = 0;
	PRAnC->a0 = a0_aux;
	PRAnC->a1 = a1_aux;
	PRAnC->a2 = a2_aux;
	PRAnC->b1 = b1_aux;
	PRAnC->b2 = b2_aux;

}

//Low pass filters butterworth
void init_butt_filter(Filter *F, float fc,float h)
{
	float a0;
	float G0=1.0;
	a0=h*fc+2.0;
	
	F->a0=a0;
	F->a1=(h*fc-2.0)/a0;
	F->a2=0.0;
	F->b0=(fc*h*G0)/a0;
	F->b1=F->b0;
	F->h=h;
	F->fc=fc;
}

void init_zero_butt_filter(Filter *F, float fc,float h)
{
	float a0;
	float G0=1.0;
	a0=h*fc+2.0;

	F->in = 0.0;
	F->in_1 = 0.0;
	F->in_2 = 0.0;
	F->out = 0.0;
	F->out_1 = 0.0;
	F->out_2 = 0.0;
	F->a0=a0;
	F->a1=(h*fc-2.0)/a0;
	F->a2=0.0;
	F->b0=(fc*h*G0)/a0;
	F->b1=F->b0;
	F->b2=0.0;
	F->h=h;
	F->fc=fc;
}


void Transform_ABC2Clarke(FORM_ABC A, FORM_CLARKE *B)
{
	//Clarke Transformation
	B->alpha = THREE_INV * (2.0*A.a - A.b - A.c);
	B->beta = SQRT_THREE_INV * (A.b - A.c);
	B->cero = A.a + A.b + A.c;
}

void Transform_Clarke2ABC(FORM_CLARKE A, FORM_ABC *B)
{
	//Inverse Clarke Transformation
	B->a = A.alpha;
	B->b = 0.5 * (-A.alpha + (SQRT_THREE * A.beta));
	B->c = 0.5 * (-A.alpha - (SQRT_THREE * A.beta));
}

void Transform_Clarke2Park(FORM_CLARKE A, FORM_PARK *B, double theta)
{
	//Park Transformation
	B->d =  (A.alpha * cos(theta)) + (A.beta * sin(theta));
	B->q = (-A.alpha * sin(theta)) + (A.beta * cos(theta));
	B->cero = A.cero;
}


void Transform_Park2Clarke(FORM_PARK A, FORM_CLARKE *B, double theta)
{
	//Inverse Park Transformation
	B->alpha = (A.d * cos(theta)) - (A.q * sin(theta));
	B->beta  = (A.d * sin(theta)) + (A.q * cos(theta));
	B->cero = A.cero;
}

//Controller Definition
double CalculatePI_tustin(PI_form *PI,float ref, float meas)
{
	PI->error_act = ref - meas;
	PI->out_act = PI->out_1 + PI->kpz*PI->error_act - PI->kpz*PI->az*PI->error_1;
	/*
	if(out_temp>PI->umax)
	{
		out_temp=PI->umax;
	}
	else if(out_temp<umin)
	{
		out_temp=umin;
	}
	else
	{
	PI->in_1=in;
	}*/
	PI->error_1 = PI->error_act;
	PI->out_1=PI->out_act;
	return (PI->out_act);
}

double CalculatePR_tpw(PR_float_form *PR, float ref, float meas)
{
	PR->error_act = ref - meas;

	PR->out_act= (PR->a2*PR->error_2) + (PR->a1*PR->error_1) + (PR->a0*PR->error_act) - (PR->b2*PR->out_2) - (PR->b1*PR->out_1);

	PR->out_2 = PR->out_1;
	PR->out_1 = PR->out_act;
	PR->error_2  = PR->error_1;
	PR->error_1  = PR->error_act;
	return(PR->out_act);

}

double CalculatePR_tpw_AnC(PR_float_AnC *PRAnC, float ref, float meas)
{
	PRAnC->error_act = ref - meas;

	PRAnC->out_act= (PRAnC->a2*PRAnC->error_2) + (PRAnC->a1*PRAnC->error_1) + (PRAnC->a0*PRAnC->error_act) - (PRAnC->b2*PRAnC->out_2) - (PRAnC->b1*PRAnC->out_1);

	PRAnC->out_2 = PRAnC->out_1;
	PRAnC->out_1 = PRAnC->out_act;
	PRAnC->error_2  = PRAnC->error_1;
	PRAnC->error_1  = PRAnC->error_act;
	return(PRAnC->out_act);

}

//Apparently this is teh low pass filter
void gen_filter(Filter *F,float in, float *out)
{
	*out=-(F->out_1)*(F->a1)-(F->out_2)*(F->a2)+in*(F->b0)+(F->in_1)*(F->b1)+(F->in_2)*(F->b2);
	F->out_2=F->out_1;
	F->out_1=*out;
	F->in_2=F->in_1;
	F->in_1=in;
	F->in=in;
	F->out=*out;
}
