// FileName: Start.c V1.0
// Autor:    Carlos Cerda Jiménez
// Date:     
// Description: . 
/*======================================================================
						CODE Initialization (Start)
========================================================================*/

//variables for transformation ab0SD
Cluster_current.ap = 0;
Cluster_current.bp = 0;
Cluster_current.cp = 0;
Cluster_current.an = 0;
Cluster_current.bn = 0;
Cluster_current.cn = 0;

V_capacitor_cluster.ap = 0;
V_capacitor_cluster.bp = 0;
V_capacitor_cluster.cp = 0;
V_capacitor_cluster.an = 0;
V_capacitor_cluster.bn = 0;
V_capacitor_cluster.cn = 0;

//Voltage control
InitializePI_tustin(&Control_Voltage_average_balance, kp_average_balance, ki_average_balance, Ts, 1000000);//Initialization for average_balance PI controller
InitializePI_tustin(&Control_Voltage_alphaSigma, kp_VaS, ki_VbS, Ts, 1000000);//Initialization used in energy balance for voltage alphaSigma
InitializePI_tustin(&Control_Voltage_betaSigma, kp_VaS, ki_VbS, Ts, 1000000);//Initialization used in energy balance for voltage betaSigma
InitializePI_tustin(&Control_Voltage_alphaDelta, kp_VabD, ki_VabD, Ts, 1000000);//Initialization used in energy balance for voltage alphaDelta
InitializePI_tustin(&Control_Voltage_betaDelta, kp_VabD, ki_VabD, Ts, 1000000);//Initialization used in energy balance for voltage betaDelta
InitializePI_tustin(&Control_Voltage_ceroDelta, kp_V0D, ki_V0D, Ts, 1000000);//Initialization used in energy balance for voltage ceroDelta

//Current control
InitializePR_tpw(&Control_Current_IaS, kp_IabS, ki_IabS, w_red, Ts); //Circulating current
InitializePR_tpw(&Control_Current_IbS, kp_IabS, ki_IabS, w_red, Ts); //Circulating current

InitializePI_tustin(&Control_Current_ACport_d, kp_Iabc_dq, ki_Iabc_dq, Ts, 1000000);//AC port current d component
InitializePI_tustin(&Control_Current_ACport_q, kp_Iabc_dq, ki_Iabc_dq, Ts, 1000000);//AC port current d component



init_butt_filter(&V_capacitor_cluster_aS,2*3.1415926535897932384626433832795*10,Ts);//Initialization for low pass filter for VcCluster alphaSigma
init_butt_filter(&V_capacitor_cluster_bS,2*3.1415926535897932384626433832795*10,Ts);//Initialization for low pass filter for VcCluster betaSigma
init_butt_filter(&V_capacitor_cluster_aD,2*3.1415926535897932384626433832795*10,Ts);//Initialization for low pass filter for VcCluster alphaDelta
init_butt_filter(&V_capacitor_cluster_bD,2*3.1415926535897932384626433832795*10,Ts);//Initialization for low pass filter for VcCluster betaDelta
init_butt_filter(&V_capacitor_cluster_0D,2*3.1415926535897932384626433832795*10,Ts);//Initialization for low pass filter for VcCluster 0Delta

init_butt_filter(&I_current_cluster_ap,2*3.1415926535897932384626433832795*1000,Ts);//Initialization for low pass filter for cluster current ap
init_butt_filter(&I_current_cluster_an,2*3.1415926535897932384626433832795*1000,Ts);//Initialization for low pass filter for cluster current an
init_butt_filter(&I_current_cluster_bp,2*3.1415926535897932384626433832795*1000,Ts);//Initialization for low pass filter for cluster current bp
init_butt_filter(&I_current_cluster_bn,2*3.1415926535897932384626433832795*1000,Ts);//Initialization for low pass filter for cluster current bn
init_butt_filter(&I_current_cluster_cp,2*3.1415926535897932384626433832795*1000,Ts);//Initialization for low pass filter for cluster current cp
init_butt_filter(&I_current_cluster_cn,2*3.1415926535897932384626433832795*1000,Ts);//Initialization for low pass filter for cluster current cn